from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.generic import ListView
from django.core.exceptions import ObjectDoesNotExist

from ad.models import Usuario
from ad.models import Actividad
from ad.models import Matricula
from ad.models import Estadio

# Create your views here.

#class administrador:
def index(request):
	#return render('index.html')
	return render_to_response('TP_logUsuario.html')
    	#return HttpResponse(output)

def principal(request):
	param_d = request.POST.get("us", "")
	if param_d == '':
		param_nom = request.POST.get("post_name", "")
		param_pass = request.POST.get("post_pass", "")
		try:
			usuario = Usuario.objects.get(nombre=param_nom,password=param_pass)
			contenido = {
				'usuario' : usuario,
		    	}
			return render_to_response('TP_menu.html',contenido)
		except ObjectDoesNotExist, e:
			contenido = {
				'mensaje' : 'El usuario no existe',
		    	}
			return render_to_response('TP_error.html', contenido)
	else:
		usuario = Usuario.objects.get(pk=param_d)
		contenido = {
			'usuario' : usuario,
	    	}
		return render_to_response('TP_menu.html',contenido)
		


def todos(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_todas.html',contenido)


def precio(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		param_precio = request.POST.get("precio", "")
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		if param_precio <> '':
			if param_precio <> 'ninguno':
				entradas = entradas.filter(precio__lte=param_precio)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_precio.html',contenido)


def estadio(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		param_estadio = request.POST.get("estadioo", "")
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		if param_estadio <> '':
			if param_estadio <> 'ninguno':
				entradas = entradas.filter(estadio__nombre__exact=param_estadio)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_estadio.html',contenido)

def matriculado(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Matricula.objects.all().filter(usuario__exact=param_d)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_matriculado.html',contenido)

def salirse(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Matricula.objects.all().filter(usuario__exact=param_d)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_salirse.html',contenido)

def ingreso(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		matriculado = Matricula.objects.all().filter(usuario__exact=param_d)
		l = [None] * 100
		for matricula in matriculado:
			entradas = entradas.exclude(pk=matricula.actividad.pk)

		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_ingreso.html',contenido)


def Ringreso(request):
	param_d = request.POST.get("us", "")
	param_a = request.POST.get("ac", "")
	if param_d <> '' and param_a <> '':
		if param_a <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			actividadn = Actividad.objects.get(pk=param_a)
			matricula = Matricula(usuario=usuarion, actividad=actividadn)
			matricula.save()
			contenido = {
				'usuario' : usuarion,
				'actividad' : actividadn,
		    	}
			return render_to_response('TP_resultado_ingreso.html',contenido)

def Rsalir(request):
	param_d = request.POST.get("us", "")
	param_m = request.POST.get("mat", "")
	if param_d <> '' and param_m <> '':
		if param_m <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			matricula = Matricula.objects.get(pk=param_m)
			actividadn = Actividad.objects.get(pk=matricula.actividad.pk)
			matricula.delete()
			contenido = {
				'usuario' : usuarion,
				'actividad' : actividadn,
		    	}
			return render_to_response('TP_resultado_salir.html',contenido)


#Administrador

def indexa(request):
	#return render('index.html')
	return render_to_response('TP_logAdmin.html')
    	#return HttpResponse(output)

def principala(request):
	param_d = request.POST.get("us", "")
	if param_d == '':
		param_nom = request.POST.get("post_name", "")
		param_pass = request.POST.get("post_pass", "")
		try:
			usuario = Usuario.objects.get(nombre=param_nom,password=param_pass)
			if usuario.admin:
				contenido = {
					'usuario' : usuario,
			    	}
				return render_to_response('TP_menuAdmin.html',contenido)
			else:
				contenido = {
					'mensaje' : 'El usuario no es administrador',
			    	}
				return render_to_response('TP_error.html', contenido)				
		except ObjectDoesNotExist, e:
			contenido = {
				'mensaje' : 'El usuario no existe',
		    	}
			return render_to_response('TP_error.html', contenido)
	else:
		usuario = Usuario.objects.get(pk=param_d)
		contenido = {
			'usuario' : usuario,
	    	}
		return render_to_response('TP_menuAdmin.html',contenido)

#Administrador Usuario

def lua(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Usuario.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_lua.html',contenido)

def eua(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Usuario.objects.all().exclude(pk=2)
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_eua.html',contenido)

def resultado_eua(request):
	param_d = request.POST.get("us", "")
	param_m = request.POST.get("mat", "")
	if param_d <> '' and param_m <> '':
		if param_m <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			matricula = Usuario.objects.get(pk=param_m)
			actividadn = 'Se ha eliminado al usuario ' + matricula.nombre
			matricula.delete()
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)

def iua(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		contenido = {
			'usuario' : usuario,
	    	}
		return render_to_response('TP_iua.html',contenido)

def resultado_iua(request):
	param_d = request.POST.get("us", "")
	param_n = request.POST.get("post_name", "")
	param_p = request.POST.get("post_pass", "")
	if param_d <> '' and param_n <> '' and param_p <> '':
		usuarion = Usuario.objects.get(pk=param_d)
		try:
			repetido = Usuario.objects.get(nombre=param_n)
			actividadn = 'El usuario ' + repetido.nombre + " ya existe "
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)
		except ObjectDoesNotExist, e:
			usuarionvo = Usuario(nombre=param_n, password=param_p, admin=0)
			usuarionvo.save()
			actividadn = 'Se ha Ingresado al usuario ' + usuarionvo.nombre
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)

#Administrador Estadio
def lea(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Estadio.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_lea.html',contenido)

def eea(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Estadio.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_eea.html',contenido)

def resultado_eea(request):
	param_d = request.POST.get("us", "")
	param_m = request.POST.get("mat", "")
	if param_d <> '' and param_m <> '':
		if param_m <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			matricula = Estadio.objects.get(pk=param_m)
			actividadn = 'Se ha eliminado al estadio ' + matricula.nombre
			matricula.delete()
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)

def iea(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		contenido = {
			'usuario' : usuario,
	    	}
		return render_to_response('TP_iea.html',contenido)

def resultado_iea(request):
	param_d = request.POST.get("us", "")
	param_n = request.POST.get("post_name", "")
	param_p = request.POST.get("post_pass", "")
	if param_d <> '' and param_n <> '' and param_p <> '':
		usuarion = Usuario.objects.get(pk=param_d)
		try:
			repetido = Estadio.objects.get(nombre=param_n)
			actividadn = 'El Estadio ' + repetido.nombre + " ya existe "
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)
		except ObjectDoesNotExist, e:
			usuarionvo = Estadio(nombre=param_n, direccion=param_p)
			usuarionvo.save()
			actividadn = 'Se ha Ingresado al estadio ' + usuarionvo.nombre
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)


#Administrador Actividad

def laa(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_laa.html',contenido)

def eaa(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		entradas = Actividad.objects.all()
		contenido = {
			'usuario' : usuario,
			'actividades' : entradas,
	    	}
		return render_to_response('TP_eaa.html',contenido)

def resultado_eaa(request):
	param_d = request.POST.get("us", "")
	param_m = request.POST.get("mat", "")
	if param_d <> '' and param_m <> '':
		if param_m <> 'ninguno':
			usuarion = Usuario.objects.get(pk=param_d)
			matricula = Actividad.objects.get(pk=param_m)
			actividadn = 'Se ha eliminado la actividad ' +matricula.descripcion+' programada el '+str(matricula.fecha)+' en el estadio '+ matricula.estadio.nombre
			matricula.delete()
			contenido = {
				'usuario' : usuarion,
				'mensaje' : actividadn,
		    	}
			return render_to_response('TP_resultadoAdmin.html',contenido)

def iaa(request):
	param_d = request.POST.get("us", "")
	if param_d <> '':
		usuario = Usuario.objects.get(pk=param_d)
		estadios = Estadio.objects.all()
		contenido = {
			'usuario' : usuario,
			'estadios' : estadios, 
	    	}
		return render_to_response('TP_iaa.html',contenido)

import datetime

def resultado_iaa(request):
	param_d = request.POST.get("us", "")
	param_n = request.POST.get("post_name", "")
	param_p = request.POST.get("post_pass", "")
	param_e = request.POST.get("estadioo", "")
	if param_d <> '' and param_n <> '' and param_p <> '' and param_e <> '':
		usuarion = Usuario.objects.get(pk=param_d)
		estadion = Estadio.objects.get(pk=param_e)
		actividadnvo = Actividad(descripcion=param_n, precio=param_p, fecha=datetime.datetime.now(), estadio=estadion)
		actividadnvo.save()
		mensaje = 'Se ha Programado la actividad ' + actividadnvo.descripcion + ' el ' + str(actividadnvo.fecha) +  'a un precio de ' + actividadnvo.precio + ' en el estadio ' + estadion.nombre
		contenido = {
			'usuario' : usuarion,
			'mensaje' : mensaje,
	    	}
		return render_to_response('TP_resultadoAdmin.html',contenido)




