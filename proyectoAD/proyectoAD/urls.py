"""proyectoAD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from ad import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^log/', views.index, name='index'),
    url(r'^principal/', views.principal, name='principal'),
    url(r'^todos/', views.todos, name='todos'),
    url(r'^precio/', views.precio, name='precio'),
    url(r'^estadio/', views.estadio, name='estadio'),
    url(r'^matriculado/', views.matriculado, name='matriculado'),
    url(r'^salirse/', views.salirse, name='salirse'),
    url(r'^ingreso/', views.ingreso, name='ingreso'),
    url(r'^resultado_ingreso/', views.Ringreso, name='Ringreso'),
    url(r'^resultado_salir/', views.Rsalir, name='Rsalir'),
    url(r'^loga/', views.indexa, name='indexa'),
    url(r'^principala', views.principala, name='principala'),
    url(r'^lua', views.lua, name='lua'),
    url(r'^iua', views.iua, name='iua'),
    url(r'^resultado_iua', views.resultado_iua, name='resultado_iua'),
    url(r'^eua', views.eua, name='eua'),
    url(r'^resultado_eua', views.resultado_eua, name='resultado_eua'),
    url(r'^lea', views.lea, name='lea'),
    url(r'^iea', views.iea, name='iea'),
    url(r'^resultado_iea', views.resultado_iea, name='resultado_iea'),
    url(r'^eea', views.eea, name='eea'),
    url(r'^resultado_eea', views.resultado_eea, name='resultado_eea'),
    url(r'^laa', views.laa, name='laa'),
    url(r'^iaa', views.iaa, name='iaa'),
    url(r'^resultado_iaa', views.resultado_iaa, name='resultado_iaa'),
    url(r'^eaa', views.eaa, name='eaa'),
    url(r'^resultado_eaa', views.resultado_eaa, name='resultado_eaa'),
]
