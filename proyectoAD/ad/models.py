from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Usuario(models.Model):
	nombre = models.CharField(max_length=255)
	password = models.CharField(max_length=255)
	admin = models.BooleanField()

class Estadio(models.Model):
	nombre = models.CharField(max_length=255)
	direccion = models.CharField(max_length=255)

class Actividad(models.Model):
	descripcion = models.CharField(max_length=255)
	precio = models.IntegerField()
	fecha = models.DateField()
	estadio = models.ForeignKey(Estadio, on_delete=models.CASCADE)

class Matricula(models.Model):
	usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
	actividad = models.ForeignKey(Actividad, on_delete=models.CASCADE)
